const low = require('lowdb')
var _ = require('lodash')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db/db.json')
const db = low(adapter)

if (db.get('queues').value() === undefined) {
    db.defaults({ queues: [] }).write()    
}

function getQueues() {
    return db.get('queues').value()
}

function addQueue(queueName) {
    let queues = db.get('queues').push(queueName).value()
    db.set('queues',_.uniq(queues)).value()
    db.write()
}

module.exports = {
    get_queues: getQueues,
    add_queue: addQueue
}