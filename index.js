var css = require('sheetify')
// var tachyons = require('tachyons')
var choo = require('choo')
var store = require('./stores/main')
const layout = require('./views/layout')
const emitBeforeView = require('./lib/EmitBeforeView')

css('tachyons')

var app = choo()
if (process.env.NODE_ENV !== 'production') {
  app.use(require('choo-devtools')())
} else {
  // Enable once you want service workers support. At the moment you'll
  // need to insert the file names yourself & bump the dep version by hand.
  // app.use(require('choo-service-worker')())
}

app.use(store)

app.route('/', layout(require('./views/main')))
app.route('/q/:queue/status/:status', emitBeforeView('load_jobs',layout(require('./views/main'))))
app.route('/app', layout(require('./views/app')))
// app.route('/*', require('./views/404'))

if (!module.parent) app.mount('body')
else module.exports = app
