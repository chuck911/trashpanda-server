const cheerio = require('cheerio')
const Minimize = require('minimize')
const minimize = new Minimize()
const {transAttr,T2S,cleanHtml,assign} = require('./util')
const {createJobs, sendToCMS,removeDuplicate,setPrimaryKey, processImages, processThumbnail, transCategory } = require('./actions')

function createListPipes(queue, spider, type) {
	return [
		cheerio.load,
		spider.parseList,
		removeDuplicate(type),
		createJobs(queue,'article', {type})
	]
}

function createArticlePipes(spider, type, extra) {
	return [
		minimize.parse.bind(minimize),
		html => cheerio.load(html, {decodeEntities: false}),
		spider.parseArticle,
		processImages(type),
		transAttr(cleanHtml),
		transAttr(T2S, ['title', 'content']),
		assign({source: extra.url, thumbnail:extra.img}),
		setPrimaryKey(type),
		sendToCMS(Object.assign({collection: type}, spider.cms))
	]
}

function createRssPipes(queue, type) {
	return [
		items => items.map(item => ({title: item.title, url: item.link})),
		removeDuplicate(type),
		createJobs(queue,'article', {type})
	]
}

function createArticlePipes2(spider, type, extra) {
	return [
		minimize.parse.bind(minimize),
		html => cheerio.load(html, {decodeEntities: false}),
		spider.parseArticle,
		processImages(type),
		transAttr(cleanHtml),
		transAttr(T2S, ['title', 'content', 'category']),
		transCategory(spider.category),
		assign({source: extra.url, _thumbnail:extra.img}),
		processThumbnail(type),
		setPrimaryKey(spider.pk||type),
		sendToCMS(Object.assign({collection: type}, spider.cms))
	]
}

module.exports = {createListPipes, createArticlePipes, createArticlePipes2, createRssPipes}