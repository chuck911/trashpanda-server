const request = require('request-promise-native')
const feedparser = require('feedparser-promised')
const spiders = require('./spiders')
const {createListPipes, createRssPipes} = require('./pipes')
const createArticlePipes = require('./pipes').createArticlePipes2
const {pipeline} = require('./util')
const Queue = require('bull')

spiders['gamme-pets'] = spiders['gamme-news']

function process(type) {
	const queue = new Queue(type)
	let spider = spiders[type]
	
	queue.process('list', 1, (job) => {
		let promise = request.get(job.data.url)
		return pipeline(promise, createListPipes(queue, spider, type))
	})

	queue.process('rss', 1, (job) => {
		let promise = feedparser.parse(job.data.url)
		return pipeline(promise, createRssPipes(queue, type))
	})
	
	queue.process('article', 1, (job) => {
		let promise = request.get(job.data.url)
		return pipeline(promise, createArticlePipes(spider, type, job.data))
	})
}

process('ipetgroup')
process('gamme-pets')
process('maoup')
// process('ipetgroup')
