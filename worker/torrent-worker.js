const Queue = require('bull')
const requestP = require('request-promise-native')
const cheerio = require('cheerio')
const fs = require('fs')
const request = require('request')
const {pipeline} = require('./util')
const spiders = require('./spiders')
const {createJobs, removeDuplicate } = require('./actions')

// const {createListPipes} = require('./pipes')

function createListPipes(queue, spider, type) {
	// let spider = spiders[type]
	return [
		cheerio.load,
        spider.parseList,
        removeDuplicate(type),
        createJobs(queue,'torrent', {type})
        // console.log 
	]
}

function process(type) {
	const queue = new Queue(type)
	let spider = spiders[type]
	
	queue.process('list', (job) => {	
		let promise = requestP.get(job.data.url)
		let result = pipeline(promise, createListPipes(queue, spider, type))
		return result
	})
	
	queue.process('torrent', (job, done) => {
        const r = request.post('https://0mag.net/upload',(err, httpResponse, body)=>{
            // console.log(err, body)
            done(err, body)
        })
        const form = r.form()
        // console.log(job.url)
        form.append('torrent_file', request.get(job.data.url))
	})
}

process('nyaa.si')

// const type = 'nyaa.si'
// const queue = new Queue('nyaa.si')
// const urls = ['https://sukebei.nyaa.si/?c=2_2&p=1025']
// createJobs(queue, 'list', {type})(urls
// 	.map((url)=>({url, title:url})))
// createJobs(queue, 'torrent', {type})([{title:'test torrent', url:'https://sukebei.nyaa.si/download/2305249.torrent'}])