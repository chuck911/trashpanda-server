const sanitizeHtml = require('sanitize-html')
const OpenCC = require('opencc')
opencc = new OpenCC('tw2sp.json')

const pipeline = (promise, pipes) => {
	for (var i = 0; i < pipes.length; i++) {
		let func = pipes[i]
		if (Array.isArray(func)) {
			promise = promise.then(parallel(func))
		} else {
			promise = promise.then(func)
		}
	}
	return promise
}

const transAttr = (func, attrs='content') => {
	if (!Array.isArray(attrs)) attrs = [attrs]
	return (article) => {
		for (let attr of attrs) {
			if (article[attr])
				article[attr] = func(article[attr])
		}
		return article
	}
}

const T2S = opencc.convertSync.bind(opencc)

const cleanHtml = (html) => {
	return sanitizeHtml(html, {
		allowedTags: [ 'h4', 'h5', 'h6', 'blockquote', 'p', 'ul', 'ol',
		'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div', 'pre','img' ]
	})
}

function assign(props) {
	return function(obj) {
		return Object.assign(obj, props)
	}
}

module.exports = {
	pipeline,
	transAttr,
	T2S,
	cleanHtml,
	assign
}