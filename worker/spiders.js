module.exports = {
	'gamebase': {
		// initUrls: () => {
		// 	let arr = [...Array(2).keys()]
		// 	return arr.map((i) => `https://www.gamebase.com.tw/news/1-PC/0-All/${i+1}/`)
		// },
		parseList: function($) {
			let $items = $('.news_list>div[id]')
			return $items.map((i,item) => {
				let $imglink = $(item).find('a').eq(0)
				let img = $imglink.find('img').attr('src')
				let title = $imglink.attr('title')
				let url = $imglink.attr('href')
				return {img, title, url}
			}).get()
		},
		parseArticle: function($) {
			$('.news_content img').each((i, img) => {
				if ($(img).data('original')) {
					let src = $(img).data('original')//.replace('https://i.gbc.tw/','http://wl-static.w6rld.com/')
					$(img).attr('src',src)
				}
			})
			let content = $('.news_content').html()
			content = content.replace(/<b>本篇新聞相關連結(.*)/, '')
			content = content.replace(/本篇新聞相關連結(.*)/, '')
			return {
				title: $('.news_title h1').eq(0).text(),
				content,
				category: $('.news_bar .category').eq(0).text(),
				date: $('.news_bar .date').eq(0).text()
			}
		}
	},
	'nyaa.si': {
		parseList: function($) {
			let $items = $('.torrent-list tr>td:nth-child(3)>a:nth-child(1)')
			return $items.map((i,item) => {
				let file = 'https://sukebei.nyaa.si'+$(item).attr('href')
				return {url:file, title:file}
			}).get()
		},
	},
	'ipetgroup': {
		pk: 'ipets',
		cms: {collection: 'pets', apiBase: 'http://headless.xinwen.stream', token: '071e84d616fe7a8939c6fd7a90ec27'},
		category: function(post){
			if (post.category.indexOf('喵')!=-1) return 'cat'
			if (post.category.indexOf('汪')!=-1) return 'dog'
			return 'other'
		},
		parseList: function($) {
			let $items = $('.articleList')
			return $items.map((i,item) => {
				let img = $(item).find('.articleImg img').attr('src')
				let $title = $(item).find('.articleTitle>a')
				let title = $title.text().trim()
				let url = 'https://ipetgroup.com'+$title.attr('href')
				return {img, title, url}
			}).get()
		},
		parseArticle: function($) {
			$('#contentWrap a.via').parent().remove()
			$('#contentWrap .activePhoto').replaceWith(function(){
				let imageId = $(this).data('picture')
				let image = `//ipetgroup.com/photo/${imageId}_0_620.jpeg`
				return $('<img/>').attr('src',image).wrap('<div></div>').parent()
			})
			$('#contentWrap section').replaceWith(function(){
				return $("<p/>").append($(this).contents());
			});
			return {
				title: $('#contentTitle').text(),
				content: $('#contentWrap').html(),
				date: $('#contentInfo>span').eq(1).text(),
				category: $('#contentInfo>a').eq(1).text()
			}
		}		
	},
	'gamme-news': {
		pk: 'ipets',
		cms: {collection: 'pets', apiBase: 'http://headless.xinwen.stream', token: '071e84d616fe7a8939c6fd7a90ec27'},
		category: function(post){
			if (post.title.indexOf('喵')!=-1 || post.title.indexOf('猫')!=-1) return 'cat'
			if (post.title.indexOf('狗')!=-1) return 'dog'
			return 'other'
		},
		parseList: function($) {
			return $('.Category-List6 h3 a').map((i, link) => ({
				title: $(link).text().trim(),
				url: $(link).attr('href')
			})).get()
		},
		parseArticle: function($) {
			$('.entry pre').remove()
			$('.photovia, .discuss').remove()
			const content = $('.entry img').each((i, img)=>{
				if ($(img).data('original')) {
					let src = $(img).data('original')
					$(img).attr('src',src)
				}
			})
			return {
				title: $('h1.title').text(),
				content: $('.entry').html(),
				date: $('.postDate').attr('content'),
				category: $('.nav>.current-cat>a').text()
			}
		}
	},
	'maoup': {
		pk: 'ipets',
		cms: {collection: 'pets', apiBase: 'http://headless.xinwen.stream', token: '071e84d616fe7a8939c6fd7a90ec27'},
		category: function(post) {
			const map = require('./lib/maoup_category')
			if (!post.category) return ''
			const _category = post.category.split(':')[1]
			if (!_category || !map[_category]) return 'topic:other'
			return 'topic:'+map[_category]
		},
		parseList: function($) {
			return $('.item-list .post-box-title a').map((i, link) => ({
				title: $(link).text().trim(),
				url: $(link).attr('href')
			})).get()
		},
		parseArticle: function($) {
			$('.addtoany_share_save_container').remove()
			$('fb\\:like').remove()
			const _content = $('.entry').html()
			const matchExtra = _content.match(/<p>(.(?!<p>))*延伸閱讀/)
			const content = matchExtra ? _content.substr(0, matchExtra.index) : _content
			const _title = $('h1').text()
			const matchCate = _title.match(/【(.*)】/)
			const title = matchCate ? _title.substr(matchCate[0].length) : _title
			const category = matchCate ? 'topic:'+matchCate[1] : undefined
			return {
				title, content, category
			}
		}
	}
}