const request = require('request-promise-native')
const spiders = require('./spiders')
const {createListPipes, createArticlePipes} = require('./pipes')
const {pipeline} = require('./util')
const Queue = require('bull')

function process(type) {
	const queue = new Queue(type)
	let spider = spiders[type]
	
	queue.process('list', 1, (job) => {	
		let promise = request.get(job.data.url)
		let result = pipeline(promise, createListPipes(queue, spider, type))
		return result
	})
	
	queue.process('article', 1 ,(job) => {	
		let promise = request.get(job.data.url)
		let result = pipeline(promise, createArticlePipes(spider, type, job.data))
		return result
	})
}

process('gamebase')
