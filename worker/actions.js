const request = require('request-promise-native')
const fs = require('fs-extra')
const sha1 = require('sha1')
const Redis = require('ioredis')
const cheerio = require('cheerio')
const shortid = require('shortid')
const mime = require('mime-types')
redis = new Redis()

const createJobs = (queue, type, extra) => {
	const jobOpt = {attempts:3, backoff:10000, timeout:20000}
	function addJob(job) {
		if (extra) Object.assign(job, extra)
		return queue.add(type, job, jobOpt)
	}

	function addJobs(jobs) {
		return Promise.all(jobs.map(addJob))
	}
	return addJobs
}

const sendToCMS = ({collection, apiBase, token}) => data => {
	// `http://cockpit.loc/api/collections/save/${collection}?token=a3ed31c8834c81a389857184e3534b`,
	apiBase = apiBase || 'http://cockpit.loc'
	token = token || 'a3ed31c8834c81a389857184e3534b'
	request({
		method: 'POST',
		uri: `${apiBase}/api/collections/save/${collection}?token=${token}`,
		// uri: `http://cms.6lu.info/api/collections/save/${collection}?token=36e53b78c7c007509450c9ab16d3e8`,
		body: {data},
		json: true
	})
	// .catch(console.error)
}

function removeDuplicate(type) {
	return function (links){
        let hashes = links.map(link => sha1(link.url))
        const set = `trashpanda:urlhashes:${type}`
		let promises = hashes.map(hash => redis.sismember(set, hash))
		return Promise.all(promises).then(exists => {
			let newLinks = []
			let newHashes = []
			exists.forEach((v,i)=>{
				if (!v){
					newLinks.push(links[i])
					newHashes.push(hashes[i])
				}
			})
			if (newHashes.length)
				redis.sadd(set, newHashes)
			return newLinks
		})
	}
}

function setPrimaryKey(keyName, init=46656) {
    return async (data) => {
        const key = `trashpanda:pk:${keyName}`
        let pk = await redis.get(key)
        console.log({pk})
        if (!pk) {
            pk = init
            await redis.set(key, pk)
        }
        let newPk = await redis.incr(key)
        return {...data, id:Number(newPk).toString(36)}
    }
}

function transCategory(categoryFunc) {
	return (data) => {
		if (!categoryFunc) return data
		data.category = categoryFunc(data)
		return data
	}
}

const prefixes = {
	gamebase: 'gb',
	ipetgroup: 'pet'
}
const imgBase = 'https://i.img2.win/'
const thumbnailBase = 'https://t.img2.win/'
const imageAPI = 'http://p.img2.win/img'

function processImages(type) {
	return (data) => {
		let $ = cheerio.load(data.content)
		let images = []
		const prefix = prefixes[type] ? prefixes[type]+'/' : ''
		let thumbnail
		$('img').replaceWith(function(){
			let url = encodeURI($(this).attr('src'))
			// urls.push(url)
			let ext = mime.extension(mime.lookup(url))
			let id = shortid.generate()
			let key = prefix+id.slice(0,2)+'/'+id.slice(2,4)+'/'+id.slice(4)+'.'+ext
			let sign = sha1(key+'|img.to.win|'+url)
			let image = `${imgBase}${key}`
			if (!thumbnail) thumbnail = {path: `${thumbnailBase}${key}`}
			images.push({url, key, sign})
			return $('<img/>').attr('src',image)
		})
		let promises = images.map(image => request({uri:imageAPI, method:'POST', json: image}))
		data.content = $.root().html()
		data.thumbnail = thumbnail
		return Promise.all(promises).then(()=>data)
	}
}

function processThumbnail(type) {
	
	return (data) => {
		if (data.thumbnail || !data._thumbnail) return data
		const url = encodeURI(data._thumbnail)
		const prefix = prefixes[type] ? prefixes[type]+'/' : ''
		let ext = mime.extension(mime.lookup(url))
		let id = shortid.generate()
		let key = prefix+id.slice(0,2)+'/'+id.slice(2,4)+'/'+id.slice(4)+'.'+ext
		let sign = sha1(key+'|img.to.win|'+url)
		data.thumbnail = {path: `${imgBase}${key}`}
		return request({url:imageAPI, method:'POST', json: {url, key, sign}}).then(()=>data)
	}
}

module.exports = {createJobs, sendToCMS, removeDuplicate, setPrimaryKey, processImages, processThumbnail, transCategory }