const setTimeout = require('timers').setTimeout;

const JSONBird = require('jsonbird');
const express = require('express');
const pump = require('pump')
const {createServer: createWebSocketServer} = require('websocket-stream');
const Queue = require('bull')
const EventEmitter = require('events').EventEmitter
const queueManage = require('./methods/queueManage')
const BatchGenerator = require('./lib/BatchGenerator')

const app = express();
app.get('/', (request, response) => response.end('Hi!'));

let testqueue = new Queue('testqueue2')
testqueue.process('ss',(job)=>{
	return new Promise(resolve => setTimeout(resolve, 0, 'ko! '+job.id))
})

function QueueGetter() {
	const emitter = new EventEmitter()
	const queues = {}
	function getQueue(queueName) {
        if (!queues[queueName]) {
			queues[queueName] = new Queue(queueName)
			emitable(queues[queueName], queueName)
        }
        return queues[queueName]
	}
	function emitable(queue, queueName) {
		// {waiting:[], active:[], delayed:[], completed:[], failed:[]}
		const events = ['error','active','stalled','progress','completed','failed','paused','resumed','cleaned']
		events.forEach((eventName)=>{
			queue.on('global:'+eventName, function(){
				let args = Array.from(arguments)
				args.unshift(queueName)
				args.unshift(eventName)
				args.unshift('queue-update')
				emitter.emit.apply(emitter, args)
			})	
		})
    }
	return {getQueue, emitter}
}
const {getQueue, emitter} = QueueGetter()
const repeatJobMethods = require('./repeatJobMethods')(getQueue)

function Bull() {
    function queueMethod(name) {
        return function() {
            let args = Array.from(arguments)
            let queueName = args.shift()
            let queue = getQueue(queueName)
            return queue[name].apply(queue, args)
        }
	}
	
	function jobMethod(name) {
		return function(queueName, jobId, ...args) {
			let queue = getQueue(queueName)
			return queue.getJob(jobId)
				.then(job => job[name](...args))
		}
	}

	function addJob(queueName, name, data, ...args) {
		let queue = getQueue(queueName)
		var batchUrls = new BatchGenerator(data.url)
		let promises = []
		for (let url of batchUrls.getURLs()) {
			// data.url = url
			// console.log(url)
			
			promises.push(queue.add(name, {...data, url}, ...args))
		}
		return Promise.all(promises)
	}

    return {
        methods: {
			queue_add: addJob,
			//queueMethod('add'),
			queue_pause: queueMethod('pause'),
			queue_resume: queueMethod('resume'),
			queue_count: queueMethod('count'),
			queue_empty: queueMethod('empty'),
			queue_close: queueMethod('close'),
			queue_getjob: queueMethod('getJob'),
			queue_remove_repeatable: queueMethod('removeRepeatable'),
			queue_get_waiting: queueMethod('getWaiting'),
			queue_get_active: queueMethod('getActive'),
			queue_get_delayed: queueMethod('getDelayed'),
			queue_get_completed: queueMethod('getCompleted'),
			queue_get_failed: queueMethod('getFailed'),
			queue_counts_all: queueMethod('getJobCounts'),
			queue_count_completed: queueMethod('getCompletedCount'),
			queue_count_failed: queueMethod('getFailedCount'),
			queue_count_delayed: queueMethod('getDelayedCount'),
			queue_count_active: queueMethod('getActiveCount'),
			queue_count_waiting: queueMethod('getWaitingCount'),
			queue_count_paused: queueMethod('getPausedCount'),
			job_progress: jobMethod('progress'),
			job_get_state: jobMethod('getState'),
			job_update: jobMethod('update'),
			job_remove: jobMethod('remove'),
			job_retry: jobMethod('retry'),
			job_discard: jobMethod('discard'),
			job_promote: jobMethod('promote'),
			job_finished: jobMethod('finished'),
        }
    }
}

const bull = Bull()

const server = app.listen(1234);
const webSocketServer = createWebSocketServer({server}, wsStream => {
	const rpc = new JSONBird({
		readableMode: 'json-message',
		writableMode: 'json-stream',
	});

	rpc.methods(bull.methods)
	rpc.methods(repeatJobMethods)
	rpc.methods(queueManage)

	const onQueueUpdate = function(...args){
		// console.log('complate222',queue, job, result)
		// console.log('onQueueUpdate')
		rpc.notify('queue-update', ...args)
	}
	emitter.on('queue-update', onQueueUpdate)

	pump(wsStream, rpc, (err)=>{
		emitter.removeListener('queue-update', onQueueUpdate)
	})
	pump(rpc, wsStream)
});