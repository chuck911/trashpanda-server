const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db/db.json')
const db = low(adapter)
let getQueue

if (db.get('repeatJobs').value() === undefined) {
    db.defaults({ repeatJobs: {} }).write()    
}

function getRepeatJobs(queueName) {
    return db.get('repeatJobs.'+queueName).value()
}

function addRepeatJob(queueName, job) {
    db.set(`repeatJobs.${queueName}.${job.name}`, {name:job.name, opts: job.opts}).value()
    db.write()
}

function removeRepeatJob(queueName, job) {
    //job.name, job.opts.repeat
    getQueue(queueName).removeRepeatable(job.name,job.opts.repeat)
        .then(()=>{
            db.unset(`repeatJobs.${queueName}.${job.name}`).value()
            db.write()
        })
}

module.exports = function (queueGetter) {
    getQueue = queueGetter
    return {
        get_repeat_jobs: getRepeatJobs,
        add_repeat_job: addRepeatJob,
        remove_repeat_job: removeRepeatJob
    }
}

// let queueName = 'testqueue'

// removeRepeatJob(queueName, {name:'ss', opts:{"attempts": 1,
// "delay": 0,
// "timestamp": 1511857154192}})

// // setRepeatJob(queueName, {name:'ss', opts:{"attempts": 1,
// // "delay": 0,
// // "timestamp": 1511857154192}})
// console.log(
//     getRepeatJobs(queueName)
// )