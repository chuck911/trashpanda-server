module.exports = wrapper

let route = null
function wrapper (eventName, view) {
	return function(state, emit) {
        var newRoute = window.location.href
        console.log(newRoute, route, newRoute != route)
        if (newRoute != route) {
            route = newRoute
            console.log('EmitBeforeView:emit', eventName)
            emit(eventName)
        }
		return view(state, emit)
	}
}