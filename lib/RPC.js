const JSONBird = require('jsonbird');

function RPC(uri) {	
	const {WebSocket} = window;
	
	const rpc = new JSONBird({
	  readableMode: 'json-message',
	  writableMode: 'json-stream',
	});
	
	const connect = (callback) => {
	  const ws = new WebSocket(uri);
	  ws.binaryType = 'arraybuffer';
	
	  const rpcOnData = str => ws.send(str);
	
	  ws.onopen = () => {
		rpc.on('data', rpcOnData);
		callback(rpc)
	  };
	  ws.onclose = () => {
		rpc.removeListener('data', rpcOnData);
	  };
	  ws.onmessage = e => {
		const data = Buffer.from(e.data);
		rpc.write(data);
	  };
	};
	return {connect}
}

module.exports = RPC